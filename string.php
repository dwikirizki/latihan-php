<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Berlatih String PHP  </h1> 
    <?php
            echo "<h3> Soal Nomer 1 </h3>" ;
            $kalimat1 = "Hello PHP" ;
            echo "Kalimat pertama : ". $kalimat1."<br>"; 
            echo "Panjang karakter kalimat pertama :". strlen($kalimat1). "<br>";
            echo "Panjang jumlah kalimat pertama : ". str_word_count($kalimat1). "<br><br>" ;

            $kalimat2 = "I'm ready for the challenges" ;
            echo "Kalimat kedua : ". $kalimat2. "<br>";
            echo "Panjang karakter kalimat kedua :". strlen($kalimat2). "<br>";
            echo "Panjang jumlah kalimat kedua :". str_word_count($kalimat2). "<br><br>";

            echo "<h3> Soal Nomer 2 </h3>" ;
            $string = "I Love PHP" ;
            echo "Kalimat Pertama Soal 2 : ". $string."<br>";
            echo "Kata Pertama :". substr($string, 0, 1). "<br>";
            echo "Kata Kedua: ". substr($string, 1, 5). "<br>";
            echo "Kata Ketiga :". substr($string, 6, 9). "<br>";

            echo "<h3> Soal Nomer 3 </h3>" ;
            $string2 = "PHP is old but Good!" ;
            echo "Kalimat Ketiga :". $string2. "<br>";
            echo "Ganti Kalimat Ketiga :".  str_replace("Good!", "awesome", $string2)."<br>";
    ?>
</body>
</html>